from os.path import abspath, dirname

# Directory information
BASE_PATH = abspath(dirname(dirname(__file__)))
DATA_PATH = BASE_PATH + '/data'
PUBLIC_PATH = BASE_PATH + '/public'
SOURCE_PATH = BASE_PATH + '/src'

# Proxy
WTF_CSRF_ENABLED = False

# General server data
DEBUG = True