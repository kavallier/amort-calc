from dateutil import parser
import json

# TODO: Probably a better way to do this. Whatevs
def json_load(path):
    data = json.load(path)

    data['startDate'] = parser.parse(data['startDate'])

    for i, j in enumerate(data['payments']):
        data['payments'][i] = datetime_parse(j)

    return data

def datetime_parse(data):
    for key, value in data.items():
        if isinstance(value, str):
            data[key] = parser.parse(value)

    return data