from flask import Flask
from .views import *
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__, static_url_path='')
app.config.from_pyfile('../configuration/config.py')

app.wsgi_app = ProxyFix(app.wsgi_app)
app.register_blueprint(views.amort.amort_view)

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=app.config['DEBUG'])
