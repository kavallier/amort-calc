from ..models.amort import Amort
from ..utils.json_util import json_load
from ..views.cross import crossdomain
from flask import Blueprint, render_template, request, current_app
from datetime import datetime

amort_view = Blueprint('amort', __name__)

# General rendering
@amort_view.route('/')
@crossdomain('*')
def main():
    return render_template(
        'index.html',
        config=current_app.config)

@amort_view.route('/calculate', methods=['GET'])
@crossdomain('*')
def calculate():
    #get vars
    p = float(request.args.get('principal'))
    r = float(request.args.get('rate'))
    t = int(request.args.get('term'))
    start = datetime.strptime(request.args.get('start-date'), '%Y-%m-%d')
    history = []

    schedule = {}
    modified_schedule = {}
    amort = Amort()

    # test extra inclusion
    # Extra payments follow a pattern: one-time, monthly, annually
    extra = {}
    if request.args.get('extra-payment') is not None and\
        request.args.get('ep-type') is not None and\
        request.args.get('ep-start') is not None:
        extra = {
            'amount': float(request.args.get('extra-payment')),
            'type': request.args.get('ep-type'),
            'date': datetime.strptime(request.args.get('ep-start'), '%Y-%m-%d')
        }

    # test historic inclusion
    # Historic data needs to account for principal-only payments made throughout the life of the loan
    # Data passed in for p and t are only useful for schedule-visual purposes; they do not influence the calculation
    # If they did, we would have a weird refinance calculator and our information would screw up
    if request.args.get('use-history') is not None:
        with open(current_app.config['DATA_PATH'] + "/history.json") as data_file:
            history = json_load(data_file)

            # New schedule takes into account additional extra payments and past extra payments against original paramters
            schedule = amort.calculate(
                history['originalAmount'],
                history['interestRate'],
                history['originalTerm'] / 12,
                history['startDate'],
                extra,
                history['payments']
            )

            # Modified schedule is a barebones amortization without historic context
            modified_schedule = amort.calculate(
                history['originalAmount'],
                history['interestRate'],
                history['originalTerm'] / 12,
                history['startDate'],
                {},
                [],
                False
            )
    else:
        # If not historic, basic amortizations are calculated
        schedule = amort.calculate(p, r, t, start, extra, history)

        # Comparison schedule does not use extra payments in calculation
        modified_schedule = amort.calculate(p, r, t, start, {}, [], False)

    # Compare both schedules and update original
    schedule = amort.compare(schedule, modified_schedule)

    return render_template('amort.html',
                           config=current_app.config,
                           schedule=schedule)


@amort_view.after_request
def add_header(response):
    if current_app.config['DEBUG']:
        response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
        response.headers['Cache-Control'] = 'public, max-age=0'

    return response
