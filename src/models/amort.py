import calendar
from datetime import datetime
from dateutil import relativedelta
import math

class Amort:
    date_format = '%m-%y'
    percent_suffix = '%'
    months = 12

    # Method © StackOverflow
    # Question - https://stackoverflow.com/q/4130922
    # Answer - https://stackoverflow.com/a/4131114
    # Author - https://stackoverflow.com/users/309848/jargalan
    # Solver - https://stackoverflow.com/users/3171/dave-webb
    def add_month(self, date, additional_months):
        month = date.month - 1 + additional_months
        year = int(date.year + month / self.months)
        month = month % self.months + 1
        day = min(date.day, calendar.monthrange(year, month)[1])
        return datetime(year, month, day)

    # Format a number to two decimals, either as dollar or percentage amounts
    # Returns absolute value due to IEEE negative zeros (it's a thing)
    def format_number(self, amount, prefix="$", suffix="", places=2):
        format = prefix + "{:,." + str(places) + "f}" + suffix
        return format.format(math.fabs(round(amount, places)))

    # Check date and type conditions for singular or series extra payment values to add
    def get_extra_payment(self, date, extra_payment):

        if bool(extra_payment) and\
               ((extra_payment['type'] == 'one-time' and extra_payment['date'].month == date.month and extra_payment['date'].year == date.year) or\
                    (extra_payment['type'] == 'monthly' and extra_payment['date'] <= date) or\
                    (extra_payment['type'] == 'annual' and extra_payment['date'].month == 1)):
                        return extra_payment['amount']

        return 0.00

    # Tally up history payments; needs work
    def get_historic_payment(self, date, payment_set):
        payment_amount = 0.00
        for i, v in enumerate(payment_set):
            if v['interest'] == 0.00 and date.month == v['date'].month and date.year == v['date'].year:
                payment_amount += v['amount']

        return payment_amount

    def get_payment_amont(self, principal, period_rate, term):
        power_rate = pow(1 + period_rate, term)
        period_pay = power_rate * period_rate
        period_pay = period_pay / (power_rate - 1)
        return principal * period_pay

    def calculate(self, principal, rate, term, start_date, extra_payment={}, history=[], show_schedule = True):
        month_count = payments_saved = 0
        interest_paid = principal_paid = 0.00
        remaining_balance = principal
        payment_date = start_date
        period_rate = (rate / 100) / self.months
        term = term * 12

        # Create monthly period payment amount from compound period rate
        period_pay = self.get_payment_amont(principal, period_rate, term)

        schedule = []
        while month_count < term:
            payment_date = self.add_month(start_date, month_count)
            month_count += 1

            # Set remaining balance as final payment amount
            if remaining_balance < period_pay:
                period_pay = (period_rate + 1) * remaining_balance
                payments_saved = term - month_count
                month_count = term

            # Get payment components
            interest_payment = period_rate * remaining_balance
            principal_payment = period_pay - interest_payment
            additional_payment = self.get_extra_payment(payment_date, extra_payment) + self.get_historic_payment(payment_date, history)

            # Sum balances to date
            remaining_balance -= (principal_payment + additional_payment)
            principal_paid += (principal_payment + additional_payment)
            interest_paid += interest_payment

            # Append results to amortization schedule
            if show_schedule:
                schedule.append({
                    'date': payment_date.strftime(self.date_format),
                    'interest': self.format_number(interest_payment),
                    'principal': self.format_number(principal_payment),
                    'extra': self.format_number(additional_payment),
                    'periodPay': self.format_number(period_pay),
                    'interestPaid': self.format_number(interest_paid),
                    'remainingBalance': self.format_number(remaining_balance),
                    'loanPaid': self.format_number((principal_paid / principal) * 100, "", self.percent_suffix)
                })

        return {
            'paymentsSaved': payments_saved,
            'interestPaid': interest_paid,
            'maturityDate': payment_date.strftime('%b %Y'),
            'schedule': schedule
        }

    def compare(self, schedule, modified_schedule):
        #r = relativedelta.relativedelta(schedule['maturityDate'], modified_schedule['maturityDate'])
        schedule['interestSaved'] = self.format_number(schedule['interestPaid'] - modified_schedule['interestPaid'])
        #schedule['paymentsSaved'] = abs(r.years * 12 + r.months)
        schedule['oldMaturityDate'] = modified_schedule['maturityDate']

        return schedule